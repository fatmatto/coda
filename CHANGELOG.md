# Changelog

## Latest (2018-11-09)

- Fixed dequeue method. correctly sorting data before dequeuing

## 1.1.0 (2018-11-08)

- 1.1.0
- feat: 🎸 Allow to choose db name and collection name

## 1.0.2 (2018-11-08)

- 1.0.2
- feat: 🎸 Added automatic handling of expired jobs

## 1.0.1 (2018-11-08)

- Bump version to 1.0.1
- feat: 🎸 Queue now accepts a promise of a MongoDB connection
- Added uuid dependency
- Added mocha dependency
- added gitlab-ci files
- First commit
