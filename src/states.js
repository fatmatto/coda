'use strict'

const IN_QUEUE_STATE = 'queued'
const IN_PROGRESS_STATE = 'in_progress'
const COMPLETED_STATE = 'completed'
const FAILED_STATE = 'failed'

module.exports = { IN_QUEUE_STATE, IN_PROGRESS_STATE, COMPLETED_STATE, FAILED_STATE }
