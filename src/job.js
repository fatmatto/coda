'use strict'

const { IN_QUEUE_STATE } = require('./states')
const uuid = require('uuid/v4')
/**
 * Wrapper for the Job Object
 *  @class Job
 */
class Job {
  constructor (queue, data) {
    this.queue = queue
    this.uuid = data.uuid || uuid()
    this.data = data
    this.createdAt = Date.now()
    this.updatedAt = Date.now()
    this.retries = 0
    this.status = IN_QUEUE_STATE
  }
  static fromExisting (data) {

  }
  toObject () {
    return Object.assign({}, {
      uuid: this.uuid,
      data: this.data,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      retries: this.retries,
      status: this.status
    })
  }
}

module.exports = Job
