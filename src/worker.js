'use strict'

const Emitter = require('./emitter')
const Queue = require('./queue')
const debug = require('debug')('coda:worker')
/**
 * @class Worker
 */
class Worker {
  /**
   *
   * @param {Coda.Queue} queue
   */
  constructor (queue, options) {
    debug('New worker')
    // The reference to the queue (db astraction)
    if (!(queue instanceof Queue)) {
      throw new TypeError('queue must be an instance of Coda.Queue')
    }
    this.queue = queue

    // Configuration
    options = options || {}

    const defaults = {
      emitOnlyOnJob: true,
      concurrency: 1,
      interval: 1000 * 10 // Tick every 10 seconds
    }

    this.options = Object.assign(defaults, options)

    debug('Starting worker with configuration:', this.options)

    // Used to emit events
    this.emitter = new Emitter()

    // In memory list of in-progress jobs
    this.workingSet = []

    // Registering the main interval

    setInterval(async () => {
      debug('Looking for a job...')
      // If we are at full capacity, we don't take more jobs
      if (this.workingSet.length >= this.options.concurrency) {
        debug('Stopping since i am at full capacity')
        return
      }

      // Let's take the next job from the queue
      let job = await this.queue.dequeue()

      // We emit the job event only if the job is not null,
      // a null job means that there's no job to handle
      if (job !== null) {
        debug('Found a runnable job')
        this.workingSet.push(job)
        this.emitter.emit('job', job)
      } else {
        debug('No jobs found')
      }
    }, this.options.interval)
  }

  complete (job) {
    debug('Complete')
    let index = this.workingSet.findIndex(item => {
      return item.uuid === job.uuid
    })
    this.workingSet.splice(index, 1)
    return this.queue.complete(job)
  }

  fail (job) {
    debug('fail')
    let index = this.workingSet.findIndex(item => {
      return item.uuid === job.uuid
    })
    this.workingSet.splice(index, 1)
    return this.queue.fail(job)
  }

  return (job) {
    debug('return')
    let index = this.workingSet.findIndex(item => {
      return item.uuid === job.uuid
    })
    this.workingSet.splice(index, 1)
    return this.queue.return(job)
  }

  /**
   * Proxy method for @see Emitter.on
   */
  on (event, callback) {
    debug('Registering hook on event ' + event)
    return this.emitter.on(event, callback)
  }
}

module.exports = Worker
