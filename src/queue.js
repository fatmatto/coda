'use strict'

const { IN_QUEUE_STATE, IN_PROGRESS_STATE, COMPLETED_STATE, FAILED_STATE } = require('./states')
const debug = require('debug')('coda:queue')
const mongodb = require('mongodb')
const Job = require('./job')

/**
  * Database Abstraction. Allows to use a MongoDB collection as a queue.
  * @class Queue
*/
class Queue {
  /**
   * The constructor
   * @param {Object} config The Queue configuration
   * @param {String} config.name The queue name, maps to the MongoDB collection name
   * @param {mongodb.Db || Promise} config.connection A MongoDB connection instance or a promise of one
   * @param {Object} config.options An object of options
   * @param {Number} config.options.maxTimeInQueue The maximum time the job can stay in the queue regardless of the number of retries, if set to 0 it can stay forever. Default to 2 days
   * @param {Number} config.options.maxRetries 0 means no maxmimum number of retries, any greater number will fail automatically the job once it reaches that number of retries. Default to 0
   * @param {Number} config.options.maxTimeInProgress 10 minutes is the default max time in progress, after that it is considered stalled and handled. Default to 10 minutes
   * @param {Boolean} config.options.handleStalledJobs If set to true, it will periodically checks for stalled jobs and re-enqueue them.
   * @param {Boolean} config.options.stalledJobsCheckInterval Number of milliseconds that must pass between each stalled job check.
   * @param {Boolean} config.options.handleExpiredJobs  If set to true, it will periodically checks for stalled jobs and fail them.
   * @param {Boolean} config.options.expiredJobsCheckInterval  Number of milliseconds that must pass between each expired job check.
   */
  constructor (config) {
    debug('New Queue instance')
    config = config || {}

    if (!config.name) {
      throw new Error('config.name is required')
    }

    this.name = config.name

    this.connection = config.connection

    // Default values
    let defaults = {
      databaseName: this.name, // The MongoDB database name. Defaults to the queue name.
      collectionName: this.name, // The MongoDB collection name. As the databaseName, defaults to the queue name
      maxTimeInQueue: 1000 * 60 * 60 * 48, // The maximum time the job can stay in the queue regardless of the number of retries, if set to 0 it can stay forever
      maxRetries: 0, // 0 means no maxmimum number of retries, any greater number will fail automatically the job once it reaches that number of retries
      maxTimeInProgress: 1000 * 60 * 10, // 10 minutes is the default max time in progress, after that
      handleStalledJobs: true, // If set to true, it will periodically checks for stalled jobs and re-enqueue them.
      stalledJobsCheckInterval: 1000 * 60 * 10, // Every N milliseconds, check for stalled jobs
      handleExpiredJobs: true, // If set to true, it will periodically checks for stalled jobs and fail them.
      expiredJobsCheckInterval: 1000 * 60 * 60 // Every N milliseconds, check for expired jobs
    }

    config.options = config.options || {}

    // Applying arguments to defaults
    this.options = Object.assign(defaults, config.options)

    // Handling the connection
    if (this.connection instanceof Promise) {
      this.connection.then(conn => {
        this.connection = conn.db(this.options.databaseName || this.name)
      })
        .catch(error => {
          throw new Error('Error while resolving promise of a mongodb connection. Got: ' + error.message)
        })
    } else {
      if (!(config.connection instanceof mongodb.Db)) {
        throw new TypeError('connection must be an instance of Db class provided by mongodb package.')
      }
    }

    // Registering the interval to handle stalled jobs
    if (this.options.handleStalledJobs === true) {
      debug('Queue instance registerd a stalled jobs check interval')
      setInterval(() => {
        this.handleStalledJobs()
      }, this.options.stalledJobsCheckInterval)
    }

    // Registering the interval to handle expired jobs
    if (this.options.handleExpiredJobs === true) {
      debug('Queue instance registerd an expired jobs check interval')
      setInterval(() => {
        this.handleExpiredJobs()
      }, this.options.expiredJobsCheckInterval)
    }
  }

  /**
   * @returns {Promise} A promise resolving the next job to process
   */
  async dequeue () {
    debug('dequeue')
    let result = await this.connection.collection(this.name)
      .findOneAndUpdate({ status: IN_QUEUE_STATE }, // Must be a job in queue
        { $set: { status: IN_PROGRESS_STATE } }, // We mark it as in progress
        {
          returnOriginal: false,
          sort: { retries: 1, createdAt: -1 } // The most recent with least retries
        }
      )

    return result.value
  }

  /**
   * Creates a new job in the queue
   * @param {Object} data
   */
  enqueue (data) {
    debug('enqueue')
    if (!(data instanceof Job)) {
      data = new Job(this, data)
    }

    data.status = IN_QUEUE_STATE
    data.updatedAt = Date.now()
    data.createdAt = Date.now()

    return this.connection.collection(this.name)
      .insertOne(data.toObject())
  }

  /**
   * Re-enqueues the job increasing the retries attribute
    @param {Job} job Il job da riaccodare
  */
  async return (job) {
    debug('return', job)
    let result = await this.connection.collection(this.name)
      .findOneAndUpdate({ uuid: job.uuid }, // Must be a job in queue
        { $set: { status: IN_QUEUE_STATE, updatedAt: Date.now(), retries: job.retries + 1 } }, // We mark it as in queue
        {
          returnOriginal: false,
          sort: { createdAt: -1, retries: 1 } // The most recent with least retries
        }
      )

    return result.value
  }

  /**
   * Sets the job status to failed
    @param {Job} job Il job da riaccodare
  */
  async fail (job, error) {
    debug('fail', job)
    let result = await this.connection.collection(this.name)
      .findOneAndUpdate({ uuid: job.uuid }, // Must be a job in queue
        { $set: { status: FAILED_STATE, updatedAt: Date.now(), error: error } }, // We mark it as in progress
        {
          returnOriginal: false
        }
      )

    return result.value
  }

  /**
   * Sets the job status to completed
    @param {Job} job Il job da riaccodare
  */
  async complete (job) {
    debug('complete', job)
    let result = await this.connection.collection(this.name)
      .findOneAndUpdate({ uuid: job.uuid }, // Must be a job in queue
        { $set: { status: COMPLETED_STATE, updatedAt: Date.now() } }, // We mark it as in progress
        {
          returnOriginal: false
        }
      )

    return result.value
  }

  /**
   * Looks for jobs that have been in queue for too long and fail them
   */
  async handleExpiredJobs () {
    debug('handleExpiredJobs')
    const threshold = Date.now() - this.options.maxTimeInQueue
    let expiredJobs = await this.connection.collection(this.name)
      .find({ status: IN_QUEUE_STATE, createdAt: { $lte: threshold } })
      .toArray()
    debug(expiredJobs.length + ' expired jobs found')
    await this.connection.collection(this.name)
      .update(
        { status: IN_QUEUE_STATE, createdAt: { $lte: threshold } },
        { $set: { status: FAILED_STATE, updatedAt: Date.now() } },
        { multi: true }
      )
    debug('Expired jobs handled.')
  }

  /**
 * Handle jobs stuck in "in_progress" due to bad worker behaviour.
 * These jobs are returned to queue.
 */
  async handleStalledJobs () {
    debug('handleStalledJobs')
    const threshold = Date.now() - this.options.maxTimeInProgress
    let stalledJobs = await this.connection.collection(this.name)
      .find({ status: IN_PROGRESS_STATE, updatedAt: { $lte: threshold } })
      .toArray()
    debug(stalledJobs.length + ' stalled jobs found')
    await this.connection.collection(this.name)
      .update(
        { status: IN_PROGRESS_STATE, updatedAt: { $lte: threshold } },
        { $set: { status: IN_QUEUE_STATE, updatedAt: Date.now() } },
        { multi: true }
      )
    debug('Stalled jobs handled.')
  }
}

module.exports = Queue
