'use strict'
/**
 * @class Emitter
 */
class Emitter {
  constructor () {
    /**
     * @type {Object}
     */
    this.listeners = {}

    /**
     * @type {Array<Object>}
     */
    this.anyListeners = []
  }

  /**
   * Registers a function to be called whenever event is fired
   * @param {String} event
   * @param {Function} handler
   */
  on (event, handler) {
    if (typeof handler !== 'function') {
      throw new TypeError('handler must be a function')
    }

    if (typeof event !== 'string') {
      throw new TypeError('event must be a string')
    }

    if (!this.listeners.hasOwnProperty(event)) {
      this.listeners[event] = []
    }

    this.listeners[event].push(handler)
  }

  /**
   * Registers a function to be called at every event
   * @param {Function} handler  The function to be invoked at any event
   */
  onAny (handler) {
    this.anyListeners.push(handler)
  }

  /**
   * Emits an event, causing every registered handler to be called.
   * @param {String} event The name of the event to trigger
   * @param {Any} data Parameter to pass to each registered handler
   */
  emit (event, data) {
    if (!this.listeners.hasOwnProperty(event)) {
      return
    }

    // Invoking the handler for the wanted event
    this.listeners[event].forEach(fn => {
      fn(data)
    })

    // Invoking the "any handlers" for the wanted event
    this.anyListeners.forEach(fn => {
      fn(data, event)
    })
  }

  /**
   * Same as emit() but wrapped in a setImmediate call in order to execute this callback
   * when the Event loop hits the "check" phase. This is useful when you don't want/need to
   * execute your event handlers right away
   * @param  {[type]} event [description]
   * @param  {[type]} data  [description]
   * @return {[type]}       [description]
   */
  emitNext (event, data) {
    setImmediate(() => {
      this.emit(event, data)
    })
  }
}

module.exports = Emitter
