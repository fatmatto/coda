'use strict'

const chai = require('chai')
const expect = chai.expect
const spies = require('chai-spies')
chai.use(spies)

const { Queue, Worker } = require('../index')
const MongoClient = require('mongodb').MongoClient
const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost:27017/test-coda'

let client = null
let connection = null

// To test setIntervals and setTimout in an async function
async function wait (ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}

describe('Worker Tests', function () {
  before(async () => {
    client = new MongoClient(MONGODB_URI)
    await client.connect()
    connection = client.db('test-coda')
  })

  it('Should queue a job, be able to pull it and then complete it', async function () {
    let coda = new Queue({
      connection: connection,
      name: 'my-test-queue'
    })

    let worker = new Worker(coda, {
      interval: 10 // 10 ms
    })

    let fn = (job) => {
    }
    const spy = chai.spy(fn)

    worker.on('job', spy)

    await coda.enqueue({
      action: 'makeAPizza',
      ingredients: ['tomato', 'mozzarella', 'sausage']
    })
    await wait(worker.options.interval * 2)
    expect(spy).to.have.been.called()
  })

  after(async () => {
    await connection.collection('my-test-queue').deleteMany({})
    client.close()
  })
})
