'use strict'

const chai = require('chai')
const { Queue, Job } = require('../index')
const MongoClient = require('mongodb').MongoClient
let expect = chai.expect
let client = null
let connection = null
const States = require('../src/states')
const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost:27017/test-coda'

describe('Queue Tests', function () {
  before(async () => {
    client = new MongoClient(MONGODB_URI)
    await client.connect()
    connection = client.db('test-coda')
  })
  it('Should require a queue name', function (done) {
    try {
      let q = new Queue() // eslint-disable-line no-unused-vars
    } catch (err) {
      expect(err).to.be.a('Error')
      expect(err.message).to.equal('config.name is required')
      done()
    }
  })
  it('Should queue a job and then be able to pull it', async function () {
    let coda = new Queue({
      connection: connection,
      name: 'my-test-queue'
    })

    await coda.enqueue({
      type: 'pulizia',
      target: 'lamacchina'
    })

    // Ora controlliamo che sia stato effettivamente inserito
    let job = await coda.dequeue()
    expect(job).to.not.equal(null)
    expect(job.data.type).to.equal('pulizia')
  })

  it('Should be able to queue an existing job', async function () {
    let coda = new Queue({
      connection: connection,
      name: 'my-test-queue'
    })

    await coda.enqueue(new Job(coda, {
      type: 'pulizia',
      target: 'lamacchina'
    }))

    // Ora controlliamo che sia stato effettivamente inserito
    let job = await coda.dequeue()
    expect(job).to.not.equal(null)
    expect(job.data.type).to.equal('pulizia')
  })

  it('Should queue a job, be able to pull it and then complete it', async function () {
    let coda = new Queue({
      connection: connection,
      name: 'my-test-queue'
    })

    await coda.enqueue({
      type: 'pulizia',
      target: 'lamacchina'
    })

    // Ora controlliamo che sia stato effettivamente inserito
    let job = await coda.dequeue()
    expect(job).to.not.equal(null)
    expect(job.data.type).to.equal('pulizia')

    job = await coda.complete(job)

    // Il job deve avere status completed
    expect(job.status).to.equal(States.COMPLETED_STATE)
  })

  it('Should queue a job, be able to pull it and then fail it', async function () {
    let coda = new Queue({
      connection: connection,
      name: 'my-test-queue'
    })

    await coda.enqueue({
      type: 'pulizia',
      target: 'lamacchina'
    })

    // Ora controlliamo che sia stato effettivamente inserito
    let job = await coda.dequeue()
    expect(job).to.not.equal(null)
    expect(job.data.type).to.equal('pulizia')

    job = await coda.fail(job)

    // Il job deve avere status completed
    expect(job.status).to.equal(States.FAILED_STATE)
  })

  it('Returned jobs should have increased retries counter', async function () {
    let coda = new Queue({
      connection: connection,
      name: 'my-test-queue'
    })

    await coda.enqueue({
      type: 'pulizia',
      target: 'lamacchina'
    })

    // Ora controlliamo che sia stato effettivamente inserito
    let job = await coda.dequeue()
    expect(job).to.not.equal(null)
    expect(job.retries).to.equal(0)

    job = await coda.return(job)

    // ci aspettiamo che il job sia di nuovo in coda ma con numero di retries maggiorato
    expect(job.status).to.equal(States.IN_QUEUE_STATE)
    expect(job.retries).to.equal(1)
  })

  after(async () => {
    await connection.collection('my-test-queue').deleteMany({})
    client.close()
  })
})
